require 'rails_helper'
RSpec.describe Image, type: :model do
    context 'association' do
      it { should belong_to(:article) }
    end

    context 'validation' do
      it { should validate_presence_of(:image_head_line) }
      it { should validate_presence_of(:menu_image) }
      it { should validate_presence_of(:url) }
      it { should validate_presence_of(:image_likes) }
    end
    it ' show image_likes' do
      image = Image.new(image_head_line: 'abc' , menu_image: 1, url: 'http://sjjjf/bc.jpg', image_likes: 15,article_id: 1)
      image.save
      expect(image.show_image_likes).to eq 15
    end

end
