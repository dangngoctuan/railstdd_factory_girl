require 'rails_helper'
RSpec.describe Text, type: :model do
    context 'association' do
      it { should belong_to(:article) }
    end
    context 'validates' do
      it { should validate_presence_of(:text_head_line) }
      it { should validate_presence_of(:menu_text) }
      it { should validate_presence_of(:message) }
      it { should validate_presence_of(:text_likes) }
    end
    it 'show text_likes' do
      text = Text.new(text_head_line: 'fssfs', menu_text: 1, message: 'fskjfsj', text_likes: 10,article_id: 1)
      text.save
      expect(text.show_text_likes).to eq 10
    end

end
