require 'rails_helper'
RSpec.describe Article, type: :model do
    context 'validation' do
      it { should have_many(:texts) }
      it { should have_many(:images) }
      it { should validate_presence_of(:title) }
      it { should validate_presence_of(:article_likes) }
    end
    context 'write test for method' do
      it 'instance of Article have method respon text and img' do
        article = Article.new(title: 'Ruby book', article_likes: 10)
        article.save
        text = Text.new(text_head_line: 'abc', menu_text: 1, message: 'dangtuan', text_likes: 20,article_id: 1)
        text.save
        img = Image.new(image_head_line: 'bcd' , menu_image: 2, url: 'http://abc/bc.jpg', image_likes: 25, article_id: 1)
        img.save
        # article.texts << text
        # article.images << img
        expect(article.by_search).to eq [text,img]
      end
      it 'article published?' do
        article = Article.new(title: 'ispublished', article_likes: 10, published: true)
        article.save
        expect(article.by_published).to eq true
      end
      it ' published fisnish ok ' do
        article = Article.new(title: 'published', article_likes: 10)
        article.save
        expect(article.by_finished).to eq article.to_s
      end
      it 'show likes' do
        article = Article.new(title: 'published', article_likes: 10)
        article.save
        expect(article.show_likes).to eq 10
      end
      it 'show menu_text' do
        article = Article.new(title: 'Ruby book', article_likes: 10)
        article.save
        text1 = Text.new(text_head_line: 'fssfs', menu_text: 1, message: 'fskjfsj', text_likes: 10,article_id: 1)
        text1.save
        text2 = Text.new(text_head_line: 'abc', menu_text: 2, message: 'dangtuan', text_likes: 20,article_id: 1)
        text2.save
        expect(article.menu_text).to eq [text2,text1]
      end
      it 'show menu_image' do
        article = Article.new(title: 'Ruby book', article_likes: 10)
        article.save
        img1 = Image.new(image_head_line: 'abc' , menu_image: 1, url: 'http://sjjjf/bc.jpg', image_likes: 15,article_id: 1)
        img1.save
        img2 = Image.new(image_head_line: 'bcd' , menu_image: 2, url: 'http://abc/bc.jpg', image_likes: 25,article_id: 1)
        img2.save
        expect(article.menu_image).to eq [img2,img1]
      end
      it ' show times now when published' do
        article = Article.new(title: 'Ruby Book', article_likes: 10, published: true)
        article.save
        expect(article.show_times).to eq true
      end

    end

end
