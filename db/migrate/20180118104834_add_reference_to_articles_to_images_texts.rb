class AddReferenceToArticlesToImagesTexts < ActiveRecord::Migration[5.1]
  def change
    add_reference :images, :article , index: true
    add_reference :texts,  :article, index: true
  end
end
